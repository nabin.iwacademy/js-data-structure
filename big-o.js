
// Four Rules of Big of Big O 

//Rule 1:  Always look for the worse case scenerio
//Rule 2: Remove constant values
//Rule 3: Different terms for inputs
//Rule 4: Drop non dominants

// Performance dependent on the size of the array.
//  As our input grew our function became slower and slower. 
// The speed of code may vary based on the cpu so we cannot say how fast the code is based on the minutes. 

// How can we sperate between good code and back code based on the effeciency.

// Big O is the language we use to tell how long it takes to run a program.

// Big O means as we keep growing the input how much the function actually slow down?

// The universal measure to tell how fast the code is Big O. 





// 1)  Big O(n) --Linear time
//___________________________________________

//Rule 1:  Always look for the worse case scenerio
//Rule 2: Remove constant values
//Rule 3: Different terms for inputs
//Rule 4: Drop non dominants

var smallArray = ["Ram", "Sita", "Gita", "rita"]

var largeArray = ["Ram", "Sita", "Gita", "rita",
"Ram", "Sita", "Gita", "rita",
"Ram", "Sita", "Gita", "rita","Ram", "Sita", "Gita", "rita","Ram", "Sita", "Gita", "rita","Ram", "Sita", "Gita", "rita"]

var veryLargeArray = new Array(10000).fill("Nepal")


function findName(array){
 let t0 = Date.now()
  //for loop
  for(let i=0; i<array.length; i++){
    if (array[i] !==""){
      console.log(" Hi ");
    }
  }
  let t1 = Date.now();
  console.log(`find Name took ${t1-t0} Milliseconds to run`)
}


findName(veryLargeArray)




//2)  Big O(1) -- Constant time 
//___________________________________________

//---> Rule 1:  Always look for the worse case scenerio
//Rule 2: Remove constant values
//Rule 3: Different terms for inputs
//Rule 4: Drop non dominants

var smallArray = ["Ram", "Sita", "Gita", "rita"]

var largeArray = ["Ram", "Sita", "Gita", "rita",
"Ram", "Sita", "Gita", "rita",
"Ram", "Sita", "Gita", "rita","Ram", "Sita", "Gita", "rita","Ram", "Sita", "Gita", "rita","Ram", "Sita", "Gita", "rita"]

var veryLargeArray = new Array(10000).fill("Nepal")

function displayFirstValue (array){
  console.log(array[0]) //O(1) 
  console.log(array[1]) //(1)
  
}
//O(2)  O(1)

//displayFirstValue(veryLargeArray)


// Calculate big(O)
//_________________________________________

//Rule 1:  Always look for the worse case scenerio
// ---> Rule 2: Ignore constants
//Rule 3: Different terms for inputs
//Rule 4: Drop non dominants


function function1(input){
  let name = "ram" //O(1)
  let address = "kathmandu" //(1)

  for (let value =0; value < input.length; value++){
    function2 (); //O(n)
    function3(); //O(n)
    let age = "26" //(n)
  }
  return age //O(1)
}

//O(3 + 3n) == O(n)






// Calculate big(O) --- O(l+m+n)
//_________________________________________

//Rule 1:  Always look for the worse case scenerio
//Rule 2: Ignore constants
// --> Rule 3: Different terms for inputs
//Rule 4: Drop non dominants


var smallArray = ["Ram", "Sita", "Gita", "rita"]

var largeArray = ["Ram", "Sita", "Gita", "rita",
"Ram", "Sita", "Gita", "rita",
"Ram", "Sita", "Gita", "rita","Ram", "Sita", "Gita", "rita","Ram", "Sita", "Gita", "rita","Ram", "Sita", "Gita", "rita"]

var veryLargeArray = new Array(50).fill("Nepal")

function function4(smallArray, largeArray, veryLargeArray ){
    smallArray.forEach(function(value){
      console.log(value);
    }); //O(l)

    largeArray.forEach(function(value){
      console.log(value);
    }); //O(m)

    veryLargeArray.forEach(function(value){
      console.log(value); //0(n)
    });

}

//O(l+m+n) O(n)

//function4(smallArray, largeArray, veryLargeArray )






// Calculate big(O) --- nexted loops O(l*m*n) --> Cubic time
//_________________________________________

//Rule 1:  Always look for the worse case scenerio
//Rule 2: Ignore constants
// --> Rule 3: Different terms for inputs
// Rule 4: Drop non dominants

var smallArray = ["Ram", "Sita", "Gita", "rita"]

var largeArray = ["Ram", "Sita", "Gita", "rita",
"Ram", "Sita", "Gita", "rita",
"Ram", "Sita", "Gita", "rita","Ram", "Sita", "Gita", "rita","Ram", "Sita", "Gita", "rita","Ram", "Sita", "Gita", "rita"]

var veryLargeArray = new Array(50).fill("Nepal")

function function5(smallArray, largeArray, veryLargeArray ){
    smallArray.forEach(function(value1){
      largeArray.forEach(function(value2){
          veryLargeArray.forEach(function(value3){
              console.log(value1, value2, value3);
          });
      });
    });
}
//O(l*m*n) (n^2)

//function5(smallArray, largeArray, veryLargeArray )






// Calculate big(O) --- O(l+m+n + l*m*n) ??
//_________________________________________

//Rule 1:  Always look for the worse case scenerio
//Rule 2: Ignore constants
// --> Rule 3: Different terms for inputs
// --> Rule 4: Drop non dominants

var smallArray = ["Ram", "Sita", "Gita", "rita"]

var largeArray = ["Ram", "Sita", "Gita", "rita",
"Ram", "Sita", "Gita", "rita",
"Ram", "Sita", "Gita", "rita","Ram", "Sita", "Gita", "rita","Ram", "Sita", "Gita", "rita","Ram", "Sita", "Gita", "rita"]

var veryLargeArray = new Array(50).fill("Nepal")

function function5(smallArray, largeArray, veryLargeArray ){
    smallArray.forEach(function(value1){
      largeArray.forEach(function(value2){
          veryLargeArray.forEach(function(value3){
              console.log(value1, value2, value3);
          });
      });
    }); //O(l*m*n)

    smallArray.forEach(function(value){
      console.log(value);
    });

    largeArray.forEach(function(value){
      console.log(value);
    });

    veryLargeArray.forEach(function(value){
      console.log(value);
    });
    //O(l+m+n)
}

//O(l+m+n + l*m*n) ==O(n^3)
//function5(smallArray, largeArray, veryLargeArray )










